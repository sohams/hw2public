##Code Adapted from https://blade6570.github.io/soumyatripathy/hdf5_blog.html

import h5py
import numpy as np
import os

dataset_path = './data'  

file_path = './Dataset.hdf5' 

hf = h5py.File(file_path, 'a')  

for bigfolder in os.listdir(dataset_path):  
    group_name = os.path.join(dataset_path, bigfolder)
    grp = hf.create_group(group_name)  

    for subfolder in os.listdir(group_name):  
        subgroupfolder = os.path.join(group_name, subfolder)

        subgrp = grp.create_group(j)  
                                      

        for image in os.listdir(subgroupfolder):   
            img_path = os.path.join(subgroupfolder, image)

            with open(img_path) as img_f:  
                data = img_f.read()

            data_np = np.asarray(data)
            
            dset = subgrp.create_dataset(image, data=data_np) 

hf.close()
