1. Are questions 1, 2, and 3 from HW1 present and answered? (Y/N)

2. Does the student demonstrate that they configured the git remotes correctly? (Y/N)
    - There should be an `origin` that is linked to `code.stanford.edu:bioe301p/21s/hw2.git`
    - There should be a private repository linked to some private space in the repository.

3. Is the code used to complete the homework available? (Y/N)

4. Is the homework fully anonymized? (scale, 1-3)

5. Did the student justity the HDF5 organization of their dataset in an understandable manner? (scale, 1-3)

6. Are the dataset shapes chosen appropriate, sensible, and well justified? (scale, 1-5)

7. Is there a reasonable explaination about why they may (or may not have) seen a data storage savings? (Y/N)

8. Was a reasonable chunk shape chosen and justified? (scale, 1-5)

9. Was the simple analysis performed explained well and understandable? (scale, 1-3)

10. Were at least three chunk shapes swept and measured? (scale, 1-3)
    - It's ok if one did not complete because it would take too long.

11. Was the `against-the-grain` chunk shape test conducted/attempted? (Y/N)
    - It is ok if it did not complete because it would take too long.

12. Was the same analysis performed with the originally distributed dataset? (Y/N)
    - It is ok if it did not complete because it would take too long.
