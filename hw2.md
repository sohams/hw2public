# 1. Dataset Properties
1. The dataset name is the NIH Collection of Chest X-rays. 
2. It was collected via X-ray of patients that have come into hospitals across the United States.  
3. It came from the NIH - National Institute of Health
4. The url is https://www.kaggle.com/nih-chest-xrays/data. 
5. The paper linked to the dataset is "ChestX-ray8: Hospital-scale Chest X-ray Database and Benchmarks on Weakly-Supervised Classification and Localization of Common Thorax Diseases." (Wang et al.) The url is https://openaccess.thecvf.com/content_cvpr_2017/papers/Wang_ChestX-ray8_Hospital-Scale_Chest_CVPR_2017_paper.pdf The full citation is Wang X, Peng Y, Lu L, Lu Z, Bagheri M, Summers RM. ChestX-ray8: Hospital-scale Chest X-ray Database and Benchmarks on Weakly-Supervised Classification and Localization of Common Thorax Diseases. IEEE CVPR 2017,
6. NIH Chest X-ray Dataset is comprised of 112,120 X-ray images with disease labels from 30,805 unique patients. There are 15 classes of diseases that are to be classified based on the X-ray images. Atelectasis, Consolidation, Infiltration, Pneumothorax, Edema, Emphysema, Fibrosis, Effusion, Pneumonia, Pleural_thickening, Cardiomegaly, Nodule Mass, and Hernia.
# 2. File Format
1. The file format is given in images (.png) files. PNG (Portable Network Graphics) is a raster-graphics file format that supports lossless data compression, built specifically for transferring images across the internet. Due to that, palettes such as 24-bit RGB, grayscale, with our without alpha encoding are supported, but print palettes such as CMYK is not. A PNG contains a single image in an structure of chunks, which encode the baisc pixels and other information. There are 5 channels for PNG - which inlude grayscale, rgb/truecolor, indexed channel which goes into a palette of colors, grayscale and alpha where alpha determines the opacity of the each pixel, and RGBA (with alpha). Due to the presence of alpha PNGs are available to be transparent. Furthermore, the compression algorithm DEFLATE, used by the PNG is a lossless data compression algorithm which is non-patented. Hence, PNGs are known for the ease of portability, completeness, losslessness, efficiency (due to compression), ease of use, interchangeability, flexibility, and is non-propietary. 
2. The Metadata for each image includes its image index, Labels, Follow-up #, Patient ID, Patient Age, Patient Gender, View Position, Original Image Size and Original Pixel Spacing.   

# 3. Structure and Encoding
1. Within the home directory of the dataset, There are 12 subdirectories, labeled with the index of the folder (1,2,3..12), each folder has 1 folder labeled images  within it. Within each of the single folders, approximately 10000 png images of the Chest X-rays are stored. The png files are organised by the patient id and the followup visit number, with the initial visit initialised at 0. Therefore, the names are <patientid_followupvisit#>. Furthermore, there is a .csv file (Data_Entry_2017) which contains the metadata of all the 112,120 chest x-ray images. 
2. The chest x-rays are PNG files of size 1024 x 1024 pixels, which are a compressed version of the DICOM data object that the X-rays were in their native format. DICOM grayscale standard display function is the greyscale encoding in DICOM that allows X-rays to be viewed the same across all medical devices. The PNG encoding is done through two steps - Filtering, and then Compression. Delta encoding, where the difference between the previous value is stored, is used as the filtering step. Delta Encoding is powerful; if the data is linearly correlated like a gradient of an image, then the values in the Delta encoding are very small as a result, which makes it easier to compress. PNG is filtered via taking some relation to the pixel to the left, the pixel above,and the pixel above-left, which are all integers. The greyscale on the PNG is encoded with a bit depth of 8 as found out from the file command (A test file is uploaded to this repository X-ray.png). After the filtering, the image is compressed using the DEFLATE Algorithm.

+ Console text snippet
`-----@rice16:~/hw1$ file "X-ray.png"
X-ray.png: PNG image data, 1024 x 1024, 8-bit grayscale, non-interlaced`

# 4. Demonstration of origin and user repositories
---@rice03:~/Bioe301P/hw2$ git remote -v
`origin	https://code.stanford.edu/bioe301p/21s/hw2.git (fetch)
`origin	https://code.stanford.edu/bioe301p/21s/hw2.git (push)
`private-origin	git@code.stanford.edu:----/hw2public.git (fetch)
`private-origin	git@code.stanford.edu:----/hw2public.git (push) 

# 5. Conversion to HDF5 

Justify the organization and number of datasets created
Justify the shape and data type of each dataset
Compare the storage size of the raw dataset to the HDF5 dataset. Is there a space savings to using HDF5? Why or why not?
1. I specifically focused on creating individual dsets for each image - given that I don't know what disease each image corresponds to. It would be incorrect to put multiple images in a single dataset as we would lose categorical accuracy. Secondly, each dataset image corresponds to a more or less a single patient, who may or may not have multiple visits. Therefore, it is much safer and more accurate to place each image into each data set (1 x 1024 x1024)
I largely followed the heirarchy of the dataset present in the original distributed data set - there are 12 groups, with one individual subgroup, which has the ~10000 datasets corresponding to the images. 

2. Side Note - An equivalent dataset hdf5 storage could have been done where the 15 disease labels are used as groups, and all images corresponding to the disease label is placed as compounded dataset - 10000x1024x1024 array dset. However, this actually requires aprior knowledge of the all the classification types of the disease for the Chest X-rays, which would render my intended learning method non-functional. In particular, deterministic Neural Networks often overfit to early data labels, which make them hard to generalise. Hence, having the classification organisation is worse for general performance. Hence, I am focusing on probabilistic neural network architecture, which instead of giving a determinate answer, will also give me a probability measure of what diseases the X-ray most likely represents, which is more accurate and will require doctor expertise in fine tuning. 

3. Image Files contain a lot of data, for example, a single RGB image has 3 times the data of the resolution of the image due to channels. I initially thought about saving the images as black and white; however a quick run through both black and white and color png shows that there is not a lot of difference in the file size. This is expected, as the PNG files are already somewhat in a black and white format, which makes this not the way to store. However, I do take advantage of the fact that my images are not in color, which make it easier to deal with. 
image size (PNG): 403884 bytes
image raw size (uncompressed):1048576 bytes
hdf5 file size black and white: 405882 bytes
hdf5 file size as image: 405882 bytes

3. There is no space-saving when using hdf5 when we compare to PNG and zip files- per image we have 2000 more bytes. Overall, over 100000 images, this adds to nearly 2 gigabytes worth of data - this is shown in the size of the hdf5 file, which is 43 GB at the end. However, when we compare to the absolute raw dataset which is 112 GB, we have an almost 1/3rd reduction, which is good. 
The reason we dont see savings from the hdf5 dataset is numpy arrays take up more space than a PNG file. This is pretty interesting. The reason behind this because the PNG file, remember is using a form of lossless compression where the delta between pixels is saved, rather than the pixel itself. In this case, its a numeric difference between using a more efficient data type for the PNG (which basically stores differences, which is a smaller gradient, and uses DEFLATE as the compression algorithm which reduces numeric data size), rather than the uint8 dtype used for numpy arrays.

# 6. Chunk Shape tuning

(I didn't realise that I could use a subset of my data - and I ended up using the entirety of my dataset at 43 GBs, which made the times ridiculously long, so I had to do a rough percentage vs time calculation as answers.) 

1. Summary Statistic to be performed - a very basic summary statistic which will be appropiate for PNN, is mean and variance of the spatial pixel values. What is the average pixel value at each row and column [i,j], what is the mean and variance, and what skewness of the distribution (quantiles) does it follow?  In particular, I believe that it will help us identify the regions in the lung where diseases in particular affect, and what "hot spots" to look for when developing a Convolutional PNN. One of the main goals from the previous hw was to create a Deep Learning Model that has a larger box window, and I believe that having a simple hot-spot assessment will help in determining the size of the window. 

Secondly, I also have other statistics build in the code - median and quantiles. I do think that simple summary statistics are particularly helpful, because it can create a performance marker for the PNN, which also outputs statistical measures. If the PNN gives values for variance that are close to the values we have from summary statistics, then the model has come close to overfitting the training data If the model gives out smaller variances, than the model has quite overfit the data, and we need more regularisation or other ways to downforce overfitting.  

Furthermore, I also tried to find out what is the average pixel value for all the data - if it tends to be skewed to a certain distribution then we know how true PNG files are compared to DICOM Files, and whether the dataset has more males/females or more of one disease than the other. If the average pixel value is normally distributed, than spatial pixel distributions may be normally distributed as well, which would be a welcome news for a PNN. 

2. Chunk Sizes - Since I am going spatial on my datasets. which is 1024 by 1024 and focusing on spatial pixel values - it would be better to create chunks that are larger on the row-size than the column-size (i.e with the grain)- mainly because since data is stored in a row major order. Therefore I chose values of 32 x 64 (122 minutes), 4 x 16 (150 minutes), 64 x 256 (136 minutes), and 128 x 512 ( > 160 minutes).  (I ran out of time on the very last). (I repackaged the my hdf5 file using hd5repack -v -l CHUNK= [size x size] file1 file2).

3. Definitely chunks that are more square and smaller and a bit more oblong tend to perform better - there is an inherent trade-off of between grabbing too many chunks when the chunk size is smaller, vs grabbing way too much data when the chunk size is larger. For this dataset, it is pretty evident that grabbing too many by one row being significantly larger than the other one is worse off because I am also trying to get the pixel data through the column, which makes having too large of rows pretty inefficient - so somewhat square rows are better. 

4. Final Chunk size - I chose a chunk value of 32 x 128 chunk size for each image as the final chunk size. The difference. between accessing with 32x64 vs accessing with 64x256 wasn't too great of a time difference, which made me think that the increase in time is solely due to the the doubling of the amount of rows, which is more "against the grain." Secondly, I would have run a test on this, but I didn't have time to finish. I think a 32x128 pixel chunk size is pretty good given that it covers about 32x128/(1024x1024) = 0.4% of each image. Furthermore,  given that the disease boxing windows for the diseases in this dataset is roughly 100x100, I say that its quicker and more fine tuned to go for a 32x128 efficient chunk size based on the results. If I went for say a 100x100, I would have a higher cost due to more rows it needs to access. I could have gone for a 16x256, or even more oblong chunks, but that that would end up spanning multiple disease boxes, which makes it less desirable for training. 
Also on chunk sizes, when placing the images in a z-stack format with 1024x1024x10000 as stated earlier as another hdf5 dataset format, it makes this chunk size expandable down the third dimension to grab multiple images that correspond to disease blots at each boxing window. Secondly, an interesting experiment can be performed where both adjusting the chunk-size and the boxing window of the disease is done simultaneously, and seeing the training results of the learning model. If it takes longer to train but returns a more accurate reading, then we would welcome a larger chunk size, and larger boxing window. If it takes less time to train, yet is more accurate for a smaller chunk size, we can refine the boxing window. If it is longer to train, but less accurate, we know that the smaller chunks is needed. 

# 7. Against the Grain tuning

1. I chose an oblong column major chunk size 512x16, the timing was greater than 120 minutes (I ran out of time to properly see what the total was).

# 8. Original Dataset
Unfortunately, I didn't have time to complete this portion. I honestly think that the original dataset would have taken a longer time because obviously it is 112 Gbs, uncompressed, and even when using the PNG dataset, I still believe that using hdf5 format would be faster and provide better access.

From a rough scaling output from Sherlock (with using 4 cpus, 1 node), I was able to grab that about 45% of the dataset was done in an hour, so I guess about 2 hours for the raw uncompressed dataset would be reasonable on a 4 cpu node system, which roughly translates to 8 hours for the entire raw dataset on a single cpu.  
