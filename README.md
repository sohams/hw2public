# HW2

HDF5 Conversion

## Goals

The goal of this homework assignment is to convert your chosen datset to HDF5 format. This will be done with h5py.

As before, homework must be completed in Markdown, pushed to a private GitLab repository, rendered to PDF, and then saved back into the repository. A zip file of the repository (including markdown, PDF, any other work files, and exclusing the .git directory) must be submitted for peer grading.

This homework page will intentionally be updated later with more questions. Do not simply fork the repository. For this assignment, clone this repository directly, and then set up a second remote with which you push your changes to. This will allow you to pull changes from the originating repository and continue to push changes to your private repository.

Just a reminder, I advise keeping datasets into `/farmshare/scratch/users/<SUNetID>` as home directories can easily get full.

Also, don't forget to set decent parameters for [`HDF5 caching`](https://docs.h5py.org/en/stable/high/file.html#chunk-cache).

## Questions

1. Copy questions 1, 2, and 3 from HW1 and place them here.
    - This is so a rough explanation of the dataset exists in HW2 again.
    - If there are edits to make to your answers of these questions, edit them in the HW1 repository before copying here.
2. Demonstrate that both origin and private user repository remotes have been configured for this homework assignment.
    - This is the output of `git remote -v`
3. Convert your dataset to HDF5 format using h5py. Ensure it is chunked, compressed, and uses scaleoffset (if applicable).
	1. Justify the organization and number of datasets created
	2. Justify the shape and data type of each dataset
	3. Compare the storage size of the raw dataset to the HDF5 dataset. Is there a space savings to using HDF5? Why or why not?
4. Chunk shape tuning
	1. Describe a simple analysis to perform that will touch a significant fraction (>30%) of the dataset and explain what it will reveal.
		- This can be as simple as performing summary statistics (min, max, mean, median, std deviation, quartiles, etc) on the dataset if no other analysis comes to mind.
	2. Time the duration of this simple analysis on three to five different chunk shapes. All chunk shapes chosen here should either be "along-the-grain" or grainless. Do not choose a chunk shape here that is "against the grain".
		- This can be done with the `timeit` or `time` python modules
                - It is ok if one choice results in very long times. Simply stating it took more than 10x longer than another more sensible chunk shape choice is ok, it does not need to run to completion.
	3. Is there an ideal range of chunk shape for the dataset for the analysis conducted? If so, what is it and why is it ideal? If not, why not?
5. Perform the same simple analysis as in the prior question but this time use a chunk shape that would result in "cutting against the grain". Compare the timing of this analysis with the timing for the best-performing chunk shape in the prior question.
    - It is ok to stop this early if this takes >10x longer than the more sensible chunk shape approach.
6. Perform the same simple analysis using the originally distributed dataset as the source.
    - It is ok to stop if this takes >10x logner than a sensible chunk shape approach.
